//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    addUrl:"../../image/add.png",
    minusUrl:"../../image/minus.png",
    showView:"",
    userList:[],
    curPoint:[],
    chgPoint:[],
    nameDisabled:false,
    recordHidden:true,
    recordNocancel:false,
    recodeBtnDisabled:true,
    tipmodal:{title:'错误',content:'请将玩家姓名填完整',successFun:'0'},
    recordError:"",
    showModalStatus: false,
    playersNum:0,
    defaultPoint:100,
    begin:"开始"
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../location/location'
    })
  },
  btn_new: function() {
    if(this.data.begin=="重新开始")
    {
        this.setData({
          "showModalStatus":true,
          "nameDisabled":false,
          "recodeBtnDisabled":false,
          "curPoint":[],
          "userList":[],
          "chgPoint":[],
          "showView":"none"
        })
    }
    else
    {
      if(this.data.userList.length<this.data.playersNum)
      {
        this.setData({
          "tipmodal.title":"错误",
          "tipmodal.content":"请将玩家姓名填完整",
          "tipmodal.successFun":0
        })
        this.showMyModal()
        return false
      }
      for(var i=0;i<this.data.userList.length;i++)
      {
        if(this.data.userList[i].name=="")
        {
          this.setData({
            "tipmodal.title":"错误",
            "tipmodal.content":"玩家姓名不能为空",
            "tipmodal.successFun":0
          })
          this.showMyModal()
          return false
        }
      }
      this.setData({
          "tipmodal.title":"提示",
          "tipmodal.content":"玩家姓名确认，是否要开始新的一局？",
          "tipmodal.successFun":1
      })
      this.showMyModal()
    }
  },
  btn_record: function() {
    console.log(this.data.userList)
    this.setData({
        recordHidden:false
    })
  },
  modalConfirm:function(){
    var p=0;
    for(var i=0;i<this.data.chgPoint.length;i++)
    {
        console.log(this.data.chgPoint[i])
        p+=parseInt(this.data.chgPoint[i])
        console.log(p)
    }
    if(p!=0)
    {
      this.setData({
        recordError:"本局得分总和不为0，输入有误，请重新输入"
      })
      return false
    }
    if(this.data.chgPoint.length<this.data.playersNum)
      return false;
    var point=new Array()
    var curPoint={},userList={}
    console.log("length=")
    console.log(this.data.chgPoint.length)
    console.log(this.data.chgPoint)
    for(var i=0;i<this.data.chgPoint.length;i++)
    {
        var tmp = "curPoint["+i+"].point"
        curPoint[tmp]=parseInt(this.data.curPoint[i].point)+parseInt(this.data.chgPoint[i])
        var tmp = "curPoint["+i+"].name"
        curPoint[tmp]=this.data.userList[i].name
        this.data.chgPoint[i]=0
        tmp = "userList["+i+"].name"
        userList[tmp]=this.data.userList[i].name
        tmp = "userList["+i+"].chgPoint"
        userList[tmp]=0
    }
    this.setData({
      recordHidden:true,
    })
    this.setData(curPoint)
    this.setData(userList)
    this.saveData()
  },
  modalCancel: function() {
    this.setData({
        recordHidden:true
    })
  },
  nameInputEvent: function (event) {
    var id = event.currentTarget.dataset.id
    var value = event.detail.value
    var param = {},curPoint={}
    var stringName = "userList["+id+"].name"
    param[stringName] = value
    var stringPoint = "userList["+id+"].chgPoint"
    param[stringPoint] = 0
    var tmp = "curPoint["+id+"].name"
    curPoint[tmp]=value
    this.setData(param)
    this.setData(curPoint)
  },
  pointInputEvent: function (event) {
    var id = event.currentTarget.dataset.id
    var value = event.detail.value
    var param = {}
    var string = "chgPoint["+id+"]"
    param[string] = value
    this.setData(param)
  },
  onLoad: function () {
    var that = this
    var storageUserList,storagechgPoint,storagecurPoint,storageBegin;
    var storageRecodeBtnDisabled
    try {
      var userList = wx.getStorageSync('userList')
      var curPoint = wx.getStorageSync('curPoint')
      var chgPoint = wx.getStorageSync('chgPoint')
      var begin = wx.getStorageSync('begin')
      var recodeBtnDisabled=wx.getStorageSync('recodeBtnDisabled')
      console.log(userList)
      console.log("curPoint")
      console.log(curPoint)
      console.log(chgPoint)
      if (userList) {
          storageUserList=userList
          storagecurPoint=curPoint
          storagechgPoint=chgPoint
          storageBegin=begin
          storageRecodeBtnDisabled=recodeBtnDisabled
      }
    } catch (e) {
      console.log("bad")
    }
    var param={}
    if(storageUserList)
    {
      param["userList"]=storageUserList
      param["curPoint"]=storagecurPoint
      param["chgPoint"]=storagechgPoint
      param["begin"]=storageBegin
      param["recodeBtnDisabled"]=storageRecodeBtnDisabled
    }
    else
    {
      param["showModalStatus"]=true,
      param["showView"]="none"
    }
    that.setData(param)
  },
  showMyModal:function(){
    var that=this
      wx.showModal({
        title: this.data.tipmodal.title,
        content: this.data.tipmodal.content,
        success: function(res) {
          if (res.confirm) {
            switch(that.data.tipmodal.successFun)
            {
              case 0:
                console.log('用户点击确定')
                break
              case 1:
                that.newGame()
            }
          }
        }
      })
  },
  newGame:function(){
    this.setData({
        nameDisabled:true,
        recodeBtnDisabled:false,
        begin:"重新开始"
    })
    console.log("haha")
    console.log(this.data.curPoint)
    this.saveData()
  },
  addPoint:function(event){
    var id = event.currentTarget.dataset.id
    var value=this.data.chgPoint[id]
    console.log(this.data.chgPoint)
    var param = {}
    var stringChg = "chgPoint["+id+"]"
    param[stringChg] = parseInt(value)+1
    this.setData(param)
    var param1 = {}
    var stringUser = "userList["+id+"].chgPoint"
    param1[stringUser] = this.data.chgPoint[id]
    this.setData(param1)
  },
  minusPoint:function(event){
    var id = event.currentTarget.dataset.id
    var value=this.data.chgPoint[id]
    var param = {}
    var stringChg = "chgPoint["+id+"]"
    param[stringChg] = parseInt(value)-1
    this.setData(param)
    var param1 = {}
    var stringUser = "userList["+id+"].chgPoint"
    param1[stringUser] = this.data.chgPoint[id]
    this.setData(param1)
  },

  powerDrawer: function (e) { 
    var currentStatu = e.currentTarget.dataset.statu;  
    this.util(currentStatu) 
    //开始新的游戏初始化设定
    var players=this.data.playersNum
    console.log("players=")
    console.log(players)
    var curPoint={},chgPoint={}
    for(var i=0;i<players;i++)
    {
        var tmp = "curPoint["+i+"].point"
        curPoint[tmp]=this.data.defaultPoint
        this.data.chgPoint[i]=0
    }
    this.setData(curPoint)
    console.log(this.data.curPoint)
    console.log(this.data.curPoint)
    this.setData({"begin":"开始","showView":""})
  }, 
  util: function(currentStatu){ 
    /* 动画部分 */ 
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({ 
      duration: 200,  //动画时长  
      timingFunction: "linear", //线性  
      delay: 0  //0则不延迟  
    });  
       
    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;  
   
    // 第3步：执行第一组动画  
    animation.opacity(0).rotateX(-100).step();  
   
    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({ 
      animationData: animation.export() 
    }) 
       
    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function () { 
      // 执行第二组动画  
      animation.opacity(1).rotateX(0).step();  
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({ 
        animationData: animation  
      }) 
         
      //关闭  
      if (currentStatu == "close") { 
        this.setData( 
          { 
            showModalStatus: false 
          } 
        );  
      } 
    }.bind(this), 200) 
     
    // 显示  
    if (currentStatu == "open") { 
      this.setData( 
        { 
          showModalStatus: true 
        } 
      ) 
    } 
  },
  playersNumEvent: function (event) {
    var value = event.detail.value
    this.setData( 
      { 
        playersNum: value 
      } 
    ) 
  },
  defaultPointEvent: function (event) {
    var value = event.detail.value
    this.setData( 
      { 
        defaultPoint: value 
      } 
    )
  },
  saveData:function(){
    wx.setStorage({
      key:"userList",
      data:this.data.userList
    })
    wx.setStorage({
      key:"curPoint",
      data:this.data.curPoint
    })
    wx.setStorage({
      key:"chgPoint",
      data:this.data.chgPoint
    })
    wx.setStorage({
      key:"begin",
      data:this.data.begin
    })
    wx.setStorage({
      key:"recodeBtnDisabled",
      data:this.data.recodeBtnDisabled
    })
  }
})
