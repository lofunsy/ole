//undercover.js
//获取应用实例
var app = getApp()
Page({
  data: {
    isOwn:false,
    showModalStatus: false,
    showModal: false,
    shadeModal:false,
    roomUserList:[],   
    room_id:0,
    xxgame_id:0,
    nick_name:"",
    room_players:0,
    max_players:[],
    players_num:0,
    has_blank:false,
    message: '',
    messageText:'',
    scrollTop: 0,
    srollHeight: 300,
    scrollHeight:0
  },
  quitRoom: function() {
    this.setData({
      showModal: true,
      shadeModal: true
    })
  },
  onConfirm: function() {
    var that=this
    wx.request({
      url: getApp().globalData.serviceAddress + '/quitRoom',
      data: {
        username: that.data.xxgame_id,
      },
      method: 'POST',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        if(res.data.status==0){
          that.hideModal();
          wx.switchTab({
            url: '../main/main'
          })
        }
      }
    })
  },
  onCancel:function(){
    this.hideModal();
  },
  modalCancel: function() {
    this.setData({
        recordHidden:true
    })
  },
  nameInputEvent: function (event) {
    
  },
  pointInputEvent: function (event) {
    
  },
  onLoad: function (option) {
    var that=this
    this.setData({
      room_id: option.room_id,
      xxgame_id: option.xxgame_id
    })
    if(option.room_owner==1){
      const max_players = []
      for (let i = 1; i <= 10; i++) {
        max_players.push(i)
      }
      this.setData({
        isOwn:true,
        showModalStatus: true,
        shadeModal:true,
        max_players: max_players
      })
    }
    // 获取系统信息
    wx.getSystemInfo({
      success: function (res) {
        console.log(res);
        // 可使用窗口宽度、高度
        console.log('height=' + res.windowHeight);
        console.log('width=' + res.windowWidth);
        // 计算主体部分高度,单位为px
        that.setData({
          // second部分高度 = 利用窗口可使用高度 - first部分高度（这里的高度单位为px，所有利用比例将300rpx转换为px）
          // second_height: res.windowHeight - res.windowWidth / 750 * 300
        })
      }
    })
    
    wx.request({
      url: getApp().globalData.serviceAddress+'/getRoomUser',
      data: {
        room_id: option.room_id,
      },
      method: 'POST',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        let info = res.data.info
        let roomUserList = [];
        let nickName="";
        for (let i in info){
          roomUserList.push({ enter_ip: info[i][0], avatarUrl: info[i][1],nickName: info[i][2],user_id:info[i][3]});
          if(info[i][3]==that.data.xxgame_id)
            nickName = info[i][2]
        }
        that.setData({
          room_players: res.data.num,
          roomUserList: roomUserList,
          nick_name: nickName
        })
      }
    })    
    this.getChatMsg()
  },
  onShow: function () {  //在onShow中根据屏幕窗口宽度动态设置scroll-view的高度
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        var height = res.windowHeight - 40;   //footerpannelheight为底部组件的高度
        that.setData({
          srollHeight: height
        });
      }
    })
  },
  showMyModal:function(){
    var that=this
      wx.showModal({
        title: that.data.tipmodal.title,
        content: that.data.tipmodal.content,
        success: function(res) {
          if (res.confirm) {
            switch(that.data.tipmodal.successFun)
            {
              case 0:
                console.log('用户点击确定')
                break
              case 1:
                that.newGame()
            }
          }
        }
      })
  },
  preventTouchMove: function () {
  },
  hideModal: function () {
    this.setData({
      showModal: false,
      shadeModal: false
    });
  },
  chatInput: function (e) {
    this.setData({
      message: e.detail.value
    })
  },
  //事件处理函数
  sendMsg: function (e) {
    var that = this
    wx.request({
      url: getApp().globalData.serviceAddress+'/message',
      data: {
        room_id: that.data.room_id,
        username: that.data.nick_name,
        content: that.data.message
      },
      method: 'POST',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        //无需操作
        that.setData({message:""})
      },
      error:function(){
        that.getChatMsg()
      }
    })

  },
  newGame:function(){
    
  },
  switch2Change:function(event){
    this.setData({
      has_blank: event.detail.value
    })
  },
  getChatMsg:function(event){
    let that =this
    wx.request({
      url: getApp().globalData.serviceAddress+'/message?room_id=' + that.data.room_id,
      method: 'GET',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        let info = res.data
        let msg = that.data.messageText
        msg=msg+info['msg']+"\n"
        that.setData({
          messageText: msg,
          // scrollTop: that.data.scrollTop + 1000
        })
        that.getChatMsg()
      }
    })
  },
  setPlayersNum:function(e){
    const val = e.detail.value
    this.setData({
      players_num:val
    })
  },
  powerDrawer: function (e) { 
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu) 
    this.setData({
      shadeModal: false
    })
  }, 
  util: function(currentStatu){ 
    /* 动画部分 */ 
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({ 
      duration: 200,  //动画时长  
      timingFunction: "linear", //线性  
      delay: 0  //0则不延迟  
    });  
       
    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;  
   
    // 第3步：执行第一组动画  
    animation.opacity(0).rotateX(-100).step();  
   
    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({ 
      animationData: animation.export() 
    }) 
       
    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function () { 
      // 执行第二组动画  
      animation.opacity(1).rotateX(0).step();  
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({ 
        animationData: animation  
      }) 
         
      //关闭  
      if (currentStatu == "close") { 
        this.setData( 
          { 
            showModalStatus: false 
          } 
        );  
      } 
    }.bind(this), 200) 
     
    // 显示  
    if (currentStatu == "open") { 
      this.setData( 
        { 
          showModalStatus: true 
        } 
      ) 
    } 
  },
  playersNumEvent: function (event) {
    var value = event.detail.value
    this.setData( 
      { 
        playersNum: value 
      } 
    ) 
  },
  defaultPointEvent: function (event) {
    var value = event.detail.value
    this.setData( 
      { 
        defaultPoint: value 
      } 
    )
  },
  saveData:function(){
    wx.setStorage({
      key:"userList",
      data:this.data.userList
    })
    wx.setStorage({
      key:"curPoint",
      data:this.data.curPoint
    })
    wx.setStorage({
      key:"chgPoint",
      data:this.data.chgPoint
    })
    wx.setStorage({
      key:"begin",
      data:this.data.begin
    })
    wx.setStorage({
      key:"recodeBtnDisabled",
      data:this.data.recodeBtnDisabled
    })
  }
})
