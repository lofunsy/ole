//index.js
//获取应用实例
var app = getApp()
Page( {

  data: {
    list:[
      {
        title:'深海逃生 47 (2016)\n',
        content:'一对姐妹潜水时被海底一个水下鲨鱼观察笼困住。她们只有一个小时的时间逃脱牢笼回到水面...',
        imgurl:'../../source/images/img1.jpg',
      },
       {
        title:'无伴奏 無伴奏 (2016)\n',
        content:'《无伴奏》讲述跟随时代潮流参与学园纷争的女高中生在和大学生恋爱后逐渐变得成熟的故事',
        imgurl:'../../source/images/img2.jpg',
      },
       {
        title:'类人猿行动 (2016)\n',
        content:'男主杰米·多南与希里安·墨菲加盟二战题材影片,影片根据真实事件改编',
        imgurl:'../../source/images/img3.jpg',
      },
       {
        title:'绝对统治 (2016)\n',
        content:'丹尼尔扮演一位年轻的联邦调查局探员，深入新纳粹组织卧底，查出密谋制作脏弹的恐怖分子。',
        imgurl:'../../source/images/img4.jpg',
      },
        ],

    date:"10月26日 周三",
    items: [ {
      message: 'foo',
    }, {
        message: 'bar'
      }],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 500
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo( {
      url: '../logs/logs'
    })
  },
  onLoad: function() {
    console.log( 'onLoad' )
    var that = this
    //调用应用实例的方法获取全局数据
    app.getUserInfo( function( userInfo ) {
      //更新数据
      that.setData( {
        userInfo: userInfo
      })
    })
  }
})
