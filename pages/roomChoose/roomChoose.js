//index.js
//获取应用实例
var app = getApp()
Page({
  data: {
    room_id:"",
  },
  roomInputEvent: function (event) {
    var param = {}
    var id = event.currentTarget.dataset.id
    var value = event.detail.value
    param["room_id"] = value
    this.setData(param)
  },
  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../location/location'
    })
  },
  btn_newRoom: function() {
    wx.request({
      url: getApp().globalData.serviceAddress+'/createRoom',
      data: {
        xxgame_id: getApp().globalData.xxgameId,
      },
      method: 'POST',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        wx.redirectTo({
          url: '../undercover/undercover?room_id=' + res.data.room_id + "&room_owner=1" + "&xxgame_id=" + getApp().globalData.xxgameId
        })
      }
    }) 
  },
  btn_joinRoom: function() {
    let that=this
    wx.request({
      url: getApp().globalData.serviceAddress+'/joinRoom',
      data: {
        xxgame_id: getApp().globalData.xxgameId,
        room_id: this.data.room_id
      },
      method: 'POST',
      header: {
        'content-type': 'application/json'
      },
      success: function (res) {
        console.log(res)
        if (res.data.status==0){
          that.room_id = res.data.room_id
          wx.redirectTo({
            url: '../undercover/undercover?room_id=' + that.room_id + "&room_owner=0" + "&xxgame_id=" + getApp().globalData.xxgameId
          })
        }
        else{
          wx.showToast({
            title: res.data.msg,
            duration: 2000
          })
        }
      }
    })
  },
  onLoad: function () {
    console.log(getApp().globalData.openid)
  },
})
