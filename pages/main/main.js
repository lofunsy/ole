var app = getApp()
Page({
  data: {
    motto: '欢迎来到加油战',
    userInfo: {},
    list:[
      {
        title:'记分',
        content:'根据特定的规则，对每局游戏进行记分',
      },
      {
        title:'谁是卧底',
        content:'风靡全国的比拼语言表述能力、知识面与想象力的游戏',
      }
        ],
  },
  gameJump: function(event) {
    var id = event.currentTarget.dataset.id
    var url_path
    switch (id)
    {
      case 0:
        url_path='../index/index'
        break
      case 1:
        url_path ='../roomChoose/roomChoose'
        break
      case 2:
        url_path='../logs/logs'
        break
    }
    wx.navigateTo({
      url: url_path
    })
  },
  onLoad: function () {
    var that = this
  	//登录
    wx.login({
      success: function (loginCode) {
        //调用request请求api转换登录凭证 
        wx.getUserInfo({
          success: function (res) {
            that.setData({ userInfo: res.userInfo })
            app.globalData.userInfo = res.userInfo
            console.log(res.userInfo.avatarUrl)
            that.update()
            var appid = 'wx71df311e49eef4a2'; //填写微信小程序appid  
            var secret = '9db8810042b1c546ebe4c2adea61daba'; //填写微信小程序secret  
            //调用服务获取openID 
            wx.request({
              url: getApp().globalData.serviceAddress+'/getOpenID',
              data: {
                appid: appid,
                secret: secret,
                grant_type: "authorization_code",
                js_code: loginCode.code,
              },
              method: 'POST',
              header: {
                'content-type': 'application/json'
              },
              success: function (res) {
                app.globalData.openid = res.data.openid;
                wx.request({
                  url: getApp().globalData.serviceAddress+'/saveUser',
                  data: {
                    open_id: getApp().globalData.openid,
                    nick_name: getApp().globalData.userInfo.nickName,
                    avatarUrl: getApp().globalData.userInfo.avatarUrl
                  },
                  method: 'POST',
                  header: {
                    'content-type': 'application/json'
                  },
                  success: function (res) {
                    app.globalData.xxgameId = res.data.xxgame_id
                  }
                })
              }
            })
          }
        })
      },
      fail: function (res) {
        console.log(res)
      }
    });
  }
})
